{
 "metadata": {
  "name": "",
  "signature": "sha256:73280d173fcfc1c5f0fd006a5839f702e7d2887dcb88795b3925b350312c7b56"
 },
 "nbformat": 3,
 "nbformat_minor": 0,
 "worksheets": [
  {
   "cells": [
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "# Plotting Cool Stuff\n",
      "\n",
      "**Materials by: Matthew Gidden**\n",
      "\n",
      "This lesson is designed to get you familiar with Python's most popular plotting library, `matplotlib`, while also introducing you to more `numpy` and `scipy` concepts as we move along."
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "## Getting Started\n",
      "\n",
      "`matplotlib` is another Python module, like `numpy`. There's lots of stuff going on in `matplotlib`, and we're just going to scratch the surface. The best way to jump in to `matplotlib` on your own is to look at their [gallery](http://matplotlib.org/gallery.html), which is a collection of examples. They provide you the example and the code required to make the example. It's great!\n",
      "\n",
      "First, let's import `matplotlib`. Specifically, we're going to import `pyplot` which lets us easily use `numpy` and `matplotlib` together."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "from matplotlib import pyplot"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Normally when you make a graph using `matplotlib`, you decide how you would like to view the plot. You generally have two choices:\n",
      "\n",
      "0. `pyplot.show()`: see the plot immediately\n",
      "0. `pyplot.savefig()`: save the plot to a file"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "IPython notebooks also have an ability to plot the graph _directly in the notebook_."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "% matplotlib inline"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "## Simple Plots\n",
      "\n",
      "matplotlib provides a very simple interface for making basic line plots. Let's try to plot a simple x, y plot of $cos$ and $sin$. First let's get some data."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "import math\n",
      "import numpy as np"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "We first need to generate points along the x-axis using `np.linspace`."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "np.linspace?"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "x = np.linspace(0, 2* math.pi, 1000)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Next, let's generate our y data. Again, we'll use `numpy` because we're numerical scientists."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "cosy = np.cos(x)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Plotting is pretty simple at this point:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "pyplot.plot(x, cosy)\n",
      "pyplot.show()"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "You can add multiple lines to the plot by adding addition x and y arguments. These additional x and y arguments can be any collection of points (including individual points). You can also change the line style."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "siny = np.sin(x)\n",
      "pyplot.plot(x, cosy, x, siny, 'r--')\n",
      "pyplot.show()"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "For a complete listing of the styles of lines, see the [`plot() documentation`](http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.plot)."
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "### Exercise:\n",
      "\n",
      "Plot the same graph, but add a phase-shifted cosine: $y = cos(x - \\frac{\\pi}{4})$. There should be 3 lines total on the graph."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "We can even add a dot where the two lines cross"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "siny = np.sin(x)\n",
      "pyplot.plot(x, cosy, x, siny, 'r--', math.pi / 4, math.sqrt(2) / 2, 'o')\n",
      "pyplot.show()"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "### Aside, Looking at Subarrays\n",
      "\n",
      "In programming, there are almost always multiple solutions to a given problem (English idiom: \"There's more than one way to skin a cat\" -- don't ask me why that's stuck around the English language). We happened to \"just know\" where these two lines would cross over because we are masters of the _unit circle_. However, we could have come to this conclusion a number of different ways by investigating the data. \n",
      "\n",
      "Let's look at a very simple example"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "a = np.array([-2, -1, 0, 1, 2])"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "\n",
      "#### Option A, `np.where`\n",
      "\n",
      "First let's use `np.where` to find outthe values of `a` which are positive. \n"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "print np.where(a >= 0)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Ah ha! This gave us an _array of indicies_ where the condition is true. One of the cool 'tricks' with numpy arrays is that can request multiple values from them."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "indicies = np.array([2, 3, 4])\n",
      "print a, a[indicies]"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Therefore, we can then get the _values_ where this condition is true by"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "a[np.where(a >= 0)]"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "#### Option B, masks\n",
      "\n",
      "A _mask_ is something you apply at an array to _hide_ parts of it. Masks come in the form of _boolean arrays_. The best way to see that is by trying it out."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "mask = np.array([True, True, False])\n",
      "print a, a[mask]"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Having to construct these by hand is a pain. Luckily there's an easier way."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "mask = a >= 0\n",
      "print mask, a[mask]"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Masks are powerful! They can even be combined using _bitwise operators_. There are a couple of bitwise operators, but I'll only mention two: `&` (which is analagous to using `and`) and `|` (which is analgous to using `or`). Again, the best way to see this is by example. Note that these require extra parentheses!!"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "mask = (a >= -1) & (a <= 1) \n",
      "print mask, a[mask]"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "mask = (a < -1) | (a > 1) \n",
      "print mask, a[mask]"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Now, back to our sine and cosine functions, we know that these two functions have the same value when the cross. Let's look at the following."
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "### Exercise\n",
      "\n",
      "Print both the indicies, the values of `cosy`, and the values of `siny` where `abs(cosy - siny) < 5e-3` is true."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Have extra time? Try to remake the plot that has the cross-over value marked using this new technique."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "## Dress Up Your Plot\n",
      "\n",
      "If we turned in the above plots for a homework assignment, we would all get a failing grade! Why? What do our axes mean, what are the lines we're plotting? We need to add more information!"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "pyplot.plot(x, x ** 2)\n",
      "pyplot.xlabel('Distance (m)')\n",
      "pyplot.ylabel('Units')\n",
      "pyplot.title('A Parabola')\n",
      "pyplot.legend(['$y = x^2$'])\n",
      "pyplot.show()"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "metadata": {},
       "output_type": "display_data",
       "png": "iVBORw0KGgoAAAANSUhEUgAAAXwAAAEVCAYAAADjHF5YAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz\nAAALEgAACxIB0t1+/AAAIABJREFUeJzt3X2clXP+x/HX6IalUrGKoihysyKxFatOtCwJZUWRyrLl\nZlchP2rXjv3ZWmxuWitUll376wY13dA0NTrd6EYphYrKIiF3xUy6nTm/Pz7X6DRN05mZc53vdZ3z\nfj4e5zHXubnO9TGmz/mez/W9Pl8QERERERERERERERERERERERERERERAaAG8BkwfT/P9wG+A5YD\ny7yf84G2SYyhTznH35+mQEESYxApV3XXAYgkQVdgBXAWcDKwpozXzAEuj7t/GTARaAwU+x2gSBAc\n5DoAkSS4FZgETAAG7Oc1WaXuvw40BOoCJwEzgQXAR0AOcLD3uh3AeOxDpDVwI7AI+6bwEdA/7j0b\nAK9hHz5TvPtgHypTgZXAO8DdZcTXwDvuAuBDYDbw0/3/J4uIZJ5TgW1Y4j4b2ArUL/WaPljCLZGF\nfTCs8O4/DPT0tqt7j3f17hcD13nbh2EJuZ53vy3wfdwxvgdO8O7/BRjnbc9hzwdRHeBt4Br2Lun8\nHhgUF+OrwJ1l/heLiGSovwMvx91/F7iv1Gv6AFvYU8N/z9unufd8FnAxlnBHA18BvbznioHj4t6r\nPnAD8GdsFF8Ud4xX4l53AvANcCiwCzvPUGIA8DzQhL1r+OdjSX4k8DHwx/39R4tUhmr4EmaHYcn3\nB+C/3mN1gNuAR4Ddca+dB3TZz/uMA6phpZtXgWPZuwRU6P1sDCwEnvbe72XsXECJ+HMBB2GJ/iDv\nveLfrxr7/tt7CDgHGIOVm6qjkquIyI9+C6xl72R6ODaa7xn3WB/2LumUthlo6W2f6u3f17tfzJ4S\n0ZXAW3H7DfGeP8g7xtfs+TYwHHjO254PDIyL722gN3uXdJaz56RyI+z8wJ/LiVmkwjSCkDDrDzwK\nxOIe+w4YAdwR91is1GtKG4yd9F0A3I+VZkrKPfH7zQA+Bd7HRvg7gM+918awk7JjsBOzjdlTg78O\nuNB7fjH2zeCFUu//Z+Bv2Anhkd5rmpUTs4iIiIgbRwEbsGlvzbGvtnOBp9h3mpyIiIRUDexr8hqg\nBTajob333EisHioiImngceAi7AKSFljts8TlwJMughIRyVR+nbTtg81lzvPul56WVojNVhARkRTx\nax5+X2z2QSfgTGxGQvxl4rWxqW/7aNasWWz9+vU+hSUikrbWs2d2WZn8GuF3ACJAR2zO8Q1Arvc4\nwCXYydt9rF+/nlgsFtrbn/70J+cxZGLsit/9LdPiLy6OsXWr+7hLbiQwjTdV8/BjwF3AA9hc5+rs\nfTm8iEio/N//QffurqOomFS0VugYtx1JwfFERHy1YQMMHAgzZriOpGJ0pW2SRSIR1yFUWphjB8Xv\nWqbEX1wMffvCgAHQqpW/MSVbEC9+inn1KBGRwBkxAsaOhXnzoHqA2k9mZWXBAXK6Er6IBEL9+vXZ\nvHmz6zACr169enz77bf7PK6ELyKhkZWVhf7tH9j+fk+JJHzV8EVEMoQSvohIhlDCFxHJEEr4IiIZ\nIkCTikREMtfatWt59913WblyJV26dOGss85K+jE0whcRCYBp06bRqFEj7rzzTv72t7/5cgyN8EVE\nAmDgQFvnftWqVRx//PG+HEMjfBGRAJk0aRJDhgzx5b114ZWIBIIuvIIpU6bQsWNHvvjiC0488cQy\nX1OVC6+U8EUkEMKa8D/88ENGjRq13+fbtm3LFVdcccD3mTRpEkOHDqVu3bpEIpH9jvKV8EUk9IKc\n8D/77DMWL17MhAkTGDt2LEVFRVx44YVEo9EKv9eUKVOoVq0a8+bN4/TTTyc3N5chQ4Zw8sknJ7S/\nWiuIiPhozZo1nHPOOWzcuBGApUuXctxxx1X4fT755BNOPfVUOnfuzMyZM+ncuTPXXHNNpd6rMjRL\nR0RCIStJ9YjKfIm44IILePDBB7n++usByM/P5+KLLwYqVtIpSeybNm2idu3a1K1bl8suu6ziAVWS\nEr6IhILras/ixYv561//CljCHzt2LAAnnHACw4YNS+g91qxZw44dO1i2bBnt27cH4LXXXuPSSy/1\nJ+hSlPBFRBLQtWtXpk2bxuzZs/nmm2846qijKvweeXl5FBQUcPTRR7N9+3ZycnJo3LixD9GWTSdt\nRSQQgnzSNj8/n1mzZjFs2DAeeOABmjRpQp8+fZzEEsRZOtWAUcBJQAzoD9QEpgEfeK8ZCUwoY18l\nfJEMFOSEv2LFCpYvX05WVhZZWVnccMMNzmIJYsK/AugC3AR0AAYCU4HDgUcPsK8SvkgGCnLCD5Ig\nJnywUX4R0BvoCPwAtMDOG6wFBgCFZeynhC+SgZTwExPUhA/wPHAlcDXQCFgBLAcGA/WAQWXso4Qv\nkoGU8BNTlYTv9yydPkADYDFwLvCZ93gOMGJ/O2VnZ/+4HYlEiEQifsUnIhJK0Wi0wlf6+jXC7wU0\nBoYBdYC3gS+B3wFLvJ+NgHvL2FcjfJEMpBF+YoJY0vkJVs5pCNTAEv8nwD+AXcDnwG9RDV9EPEr4\niQliwq8KJXyRDKSEnxg1TxMRkQNSawURCYR69eqVjFKlHPXq1av0vkH87aqkIyJJsX49tGsHM2ZA\nq1auo/GXSjoikrF27oRrr4U//CH9k32iNMIXkbR0993wwQcweXLyeukHWRAuvBIRSbnp02HCBFi+\nPDOSfaKU8EUkrWzYAH37WsI/4gjX0QSLavgikjZ27bK6/YAB4C0oJXGC+GVHNXwRqZRBg2DVKpg6\nFQ7KsOGsavgikjEmT7YyzrJlmZfsE6URvoiE3n//C23awJQp0Lat62jc0Dx8EUl7O3bANdfAffdl\nbrJPlEb4IhJqv/+9zcyZODGzp2Cqhi8iae2ll2DaNKvbZ3KyT1QQf0Ua4YvIAa1bB+eeC6+9Bmef\n7Toa91TDF5G0tG0bXH01ZGcr2VeERvgiEjr9+sF338HYsSrllFANX0TSzosvwuzZsHSpkn1FBfHX\npRG+iJRpxQro1Alefx1OP911NMHieoRfDRgFnATEgP7ADmxx82LgXeA27zkRkXJt3gxXXQUjRijZ\nV5afJ20vwxL7L4A/AEOB4cBgoD32SXSFj8cXkTRRXAw33ACdO0OPHq6jCS8/E/5koJ+33RTYDLQG\n5nqPTQc6+Xh8EUkTQ4faCP+RR1xHEm5+n7Qtwko4VwJXA7+Me64QONzn44tIyM2YASNHwpIlULOm\n62jCLRWzdPoADYA3gUPiHq8NbClrh+zs7B+3I5EIkUjEt+BEJLg++gh697Yrao85xnU0wRKNRolG\noxXax89ZOr2AxsAwoA7wNrAWq+XPAZ4G8oGXSu2nWToiwvbtcN550KuXLWgi5Utklo6fCf8nWDmn\nIVADS/xrsJk7NYFVwM3sO0tHCV9EuOkmKCzUxVWJcp3wK0sJXyTDjR4Njz0GixdDrVquowkHJXwR\nCZ2lS+HSS2HePGjRwnU04aHmaSISKl9+aRdXPfOMkr0flPBFJBB27oRf/9pm5XTt6jqa9KSSjogE\nwm232cpVOTlahLwyXPfSERFJyOjR1hBt8WIlez9phC8iTi1YAFdeCfPnw0knuY4mvHTSVkQCbeNG\nW7nq+eeV7FNBCV9EnNi+Hbp1g9tvt2mY4j+VdEQk5WIxuPFG2LoVxo/XlbTJoJO2IhJIf/87LFtm\n9Xsl+9QJ4q9aI3yRNDZ7ti1isnAhHH+862jSh07aikig/Pe/0LMn/Oc/SvYuKOGLSEp8/z106QKD\nB8OFF7qOJjOppCMivisqgssvh+OOg6eeUt3eDyrpiEgg3HOPTcMcMULJ3iXN0hERX40ZA1OnwqJF\nUKOG62gyWxA/a1XSEUkTc+ZA9+4wd67aHftNJR0RcWb9erjmGpuRo2QfDEr4IpJ0331nM3Luvx86\ndXIdjZRQSUdEkmr3brjsMmjeHJ580nU0mcNlSacG8G9gLrAY6AK0AjYCs71bd5+OLSIO3X23TcN8\n/HHXkUhpfs3SuQ74CugF1ANWAA8Aw4FHfTqmiDj2zDOQm2ttE6prDmDg+FXSOcx770LgCOBNYAbQ\nAvuQWQsM8J4vTSUdkRDKzYU+fWDePDjxRNfRZJ5ESjp+1/BrA5OBZ4FDsJH+cmAwNvIfVMY+Svgi\nIbNihZ2czcmB885zHU1mct0e+VhgIvAPYBxwOPCd91wOMGJ/O2ZnZ/+4HYlEiEQifsUoIlW0caPN\nyHnySSX7VIpGo0Sj0Qrt49cIvwEQBW7FTtACLAR+DywBfgc0Au4tY1+N8EVCoqAA2re3+fb3lvWv\nWVLGZUnnCeBq4P24x+7FTtruAj4Hfotq+CKhtXu3NURr3NhO1qpHjltBqOFXhhK+SMDFYnDbbXY1\n7bRp6pETBK5r+CKSph59FObPt5uSfXgo4YtIhbzyil1UtWAB1KnjOhqpCJV0RCRhixbZjJy8PGjV\nynU0Ek/dMkUkadatg65d4fnnlezDSglfRA5o0ya4+GL485+hc2fX0UhlKeGLSLkKCuDSS6FXL7j5\nZtfRSFWohi8i+7Vzp9XsmzTRXPug0zx8Eam04mLo3dsWM5k4Ud0vg07z8EWk0u67zy6smjVLyT5d\n6H+jiOzjiSdg8mR44w049FDX0UiyKOGLyF4mTIBHHrGraI84wnU0kkyq4YvIj2bPhu7dYeZMOPNM\n19FIRejCKxFJ2NtvW5vj8eOV7NOVEr6IsHatzbV/6im44ALX0YhflPBFMtynn8JFF9lVtL/+teto\nxE+JJPxa2HKFDYH7gSa+RiQiKfP115bsb7kFbrrJdTTit0QS/svAWcAj2GpVz/oakYikREEBXHKJ\nrVp1zz2uo5FUSCThHwpMwdagHQZU8zUiEfHd9u1w5ZXW9XLYMNfRSKokkvBrAncAbwGnAYf5GpGI\n+Gr3bujRw+bYjxyp/jiZJJGEfxdwDPAXoCOW/EUkhIqLrePlDz/Aiy9CNX1fzyiJJPyOwD3AFuBJ\noGsC+9QA/g3MBRYDXYDmwHzvsacI5kVfImkrFoO774b337dmaDVruo5IUq28pPsb4CbgVGCV99hB\nWInnQOvd9AFaAncC9YAVwHJgOJbwRwIzgJwy9tWVtiI+yM62RD9nDtSr5zoaSbaqtkc+GDgaGAI8\n6L22CPgS2HGAYx/mvb4QOAJ4E/ugONZ7/nLgIuD2MvZVwhdJsocesqUJo1Fo0MB1NOKHqrZWaAl8\nhE3LbAGchI32OyRw7K1Ysq8NvAT8odSxCoHDE3gfEamiJ56AUaOszbGSfWYrr1vmBcASoAdQesid\nl8B7HwtMBP4BjAUejnuuNnZOoEzZ2dk/bkciESKRSAKHE5HSnn0WHnvMyjiNGrmORpIpGo0SjUYr\ntI9fJ04bAFHgVmC299gUrIY/B3gayMdG/6WppCOSBP/6FwwZYh0wmzd3HY34LVlLHA7GZuls8+7H\nsGma5XkCuBp4P+6xO4ARWC1/FXAz+35zACV8kSobPx4GDoT8fDjlFNfRSCokK+GvBNoCPyQhpkQo\n4YtUweTJ0K8f5OVBy5auo5FUSdaath8C25MRkIj4KzfXLqyaPl3JXvaVSMI/GHjHu8W8W08/gxKR\ninv9dbjhBsjJgdatXUcjQVTe8L933HYMq+HXAdZhJ179opKOSAXl58O118LLL0OHRCZOS9qpaknn\nFPY+qVoLaI+dePUz4YtIBeTnWzO0V16B9u1dRyNBVtFpmYdgyb6ND7GU0AhfJEGzZkHPnpbszz/f\ndTTikh+LmG8HdlY2IBFJnpkz94zslewlERVN+A2xBVFExKG8PBvZT5yoZC+JK6+GP7bU/YOxLpl3\n+heOiBxIXh5cfz1MmgS/+IXraCRMyqv3RLCTtiWv+QFYA3zvc0yq4YvsR3yyP+8819FIkCTrSttU\nU8IXKUNurs2zV7KXsvhx0lZEHMjJ2XNRlZK9VJYSvkjAjR0L/ftbu4Rzz3UdjYSZEr5IgD33nK1D\nO2uW2iVI1SXSS0dEHHjySXj4Yetnf9JJrqORdKCELxJADz1kq1XNnQtNm7qORtKFEr5IgMRikJ1t\nC5jMnatlCSW5lPBFAiIWg0GDbK79nDlacFySTwlfJACKiuCWW2D5cohGoX591xFJOlLCF3Fsxw64\n7jrYvNkWMald23VEkq40LVPEoYIC6NzZtl97Tcle/OV3wm8DzPa2WwGfevdnA919PrZIoH39NVxw\nATRrZidpDz7YdUSS7vws6dwDXA8UevdbA496N5GM9skncNFFcNVV8OCDkBXErlaSdvwc4a8DurGn\nmU9roDO2YtZobMlEkYyzerW1Ne7XD/7yFyV7SR0/E/5EYHfc/cXA3UAH4EPgTz4eWySQ3nwTOna0\nUf3Aga6jkUyTylk6k4DvvO0cbDH0MmVnZ/+4HYlEiEQifsYlkhIzZkCvXjBmDHTp4joaCbtoNEo0\nGq3QPn5/mWyKrZzVDlgI/B5YAvwOaATcW8Y+6ocvaee552DwYFuSUB0vxQ+J9MNPxQi/JHv3B/4B\n7AI+B36bgmOLOBWLwQMPwL/+ZVfPtmjhOiLJZEE8XaQRvqSFXbvsxOzKlfDqq2qVIP4KyghfJOMU\nFMDVV0O1atYqoZbmpEkA6EpbkST7/HPo0AGOOw4mT1ayl+BQwhdJotWroV07u6DqmWegur5DS4Do\nz1EkSV5/HXr0sFWqevd2HY3IvjTCF0mC0aMt2Y8bp2QvwaURvkgVFBXB//wPTJkC8+Zp7VkJNiV8\nkUoqLISePe3nokVatESCTyUdkUrYsMEaoDVoALm5SvYSDkr4IhX05pvQtq31xXn2WahZ03VEIolR\nSUekAiZMgNtvt5O0l1/uOhqRilHCF0lAURHcfz/85z+Qlwdnnuk6IpGKU8IXOYAtW2yR8R9+gCVL\n4Kc/dR2RSOWohi9SjtWroU0bW3c2L0/JXsJNCV9kP6ZMsZ44994LI0ZAjRquIxKpGpV0REopLra1\nZp99FqZOtRG+SDpQwheJU1BgrRG++MKmXx59tOuIRJJHJR0Rz3vvwTnnWJ1+9mwle0k/SvgiwIsv\nQiRi684+8wwcfLDriESSTyUdyWg7dsDAgTBrFuTnQ8uWriMS8Y8SvmSsjz+2ZQiPPdbm1x9+uOuI\nRPzld0mnDTDb224OzAfmAk8RzAXUJUNMnw4//zlccw28/LKSvWQGPxP+PcAooKQa+igwGGiPJfsr\nfDy2SJl274Y//hFuvtkS/V13QZaGHpIh/Ez464Bu7BnJn4WN7gGmA518PLbIPjZsgI4dYeFCWLoU\nzj/fdUQiqeVnwp8I7I67Hz+OKgT0JVpSZvJkOPtsuPRSa5HQsKHriERSL5UnbYvjtmsDW/b3wuzs\n7B+3I5EIkUjEt6AkvW3fDoMGwbRpkJMD7dq5jkgkOaLRKNFotEL7+F29bAqMBdoBU4DhwBzgaSAf\neKmMfWKxWMznsCQTvP++nZRt3tz619et6zoiEf9k2cmocnN6Ki68KsnedwEPAAuwbxYvp+DYkoFi\nMXjhBVuC8JZb4KWXlOxFIJhTIzXCl0r79lu49VZ45x0YPx5+9jPXEYmkRlBG+CIpMXMmnHGGnZBd\nulTJXqQ0XWkrobdtm/WsnzgR/vlP6KQJvyJl0ghfQm3ZMmjdGjZtghUrlOxFyqOEL6FUVARDh8Kv\nfmVXzo4bB/Xru45KJNhU0pHQef99uPFGOOQQeOsta34mIgemEb6ERlERPPKITbe89lo7SatkL5I4\njfAlFFatgr59oVYtW3rw+ONdRyQSPhrhS6Dt3m21+g4drIwza5aSvUhlaYQvgbVypY3qjzzSavXH\nHec6IpFw0whfAmfbNhgyBC68EG67DXJzlexFkkEJXwIlLw9OPx3WrbMR/o03aoESkWRRSUcCYdMm\nuPNOWLAAnnoKLrnEdUQi6UcjfHGquBiefdZG9Y0bw3vvKdmL+EUjfHFm5UrrbLl7t82+adnSdUQi\n6U0jfEm5zZvhd7+zvjfXX29lHCV7Ef8p4UvKFBXBqFFwyik2ql+9Gvr3h4P0VyiSEirpSEosWgS3\n3279b6ZPh1atXEckknmU8MVXX3xhvepnzoSHHoLrrtM0SxFX9GVafLF1K/zv/8Jpp8FRR8GaNVav\nV7IXcUcJX5KqqMhWnWrRwhqeLV0KDz8MtWu7jkxEXJR0lgHfedsfAr9xEIP4YOZMuPtuS+6vvAJt\n2riOSETipTrhH+L97Jji44qP3n0XBg2ydggPPQRdu6p0IxJEqS7pnAEcCswA8gGNAUNs3To7CXvh\nhbbU4HvvQbduSvYiQZXqhL8VeAS4GOgP/MdBDFJFn3wCN98MbdvanPp16+COO6BmTdeRiUh5Ul3S\n+QBY522vBb4BjgY2xr8oOzv7x+1IJEIkEklNdFKuTZtsMZIXX4R+/eCDD7RwuIgr0WiUaDRaoX1S\n/eW7H9ASuA04BivrnAYUx70mFovFUhyWlOerr2D4cLtKtlcvuO8+aNDAdVQiEi/Laqnl5vRUl1PG\nAHWAucA4oC97J3sJkM8+s5bFLVrAli2wfDk8/riSvUhYpbqksxvoleJjSgV9/LHNthk3Dnr3hnfe\ngUaNXEclIlWlE6byo7VrbYWps86Cww+3q2Mfe0zJXiRdqJeOsHCh1ejnzLEGZ2vX6mSsSDpSws9Q\nRUWQk2OJ/osvYMAAeP55qFXLdWQi4hcl/Ayzdav1unnsMWtqdtdddmVstWquIxMRvynhZ4h16+Dp\np+GFF6B9e/j3v+Hcc11HJSKppJO2aayoCKZOtbYH7drZKH7xYmtspmQvknk0wk9DX30FY8bYiL5h\nQ1soPCfHVpsSkcylhJ8mioogLw+ee87aFHfrZiP51q1dRyYiQRHEvoZqrVAB69bZSdgXXoBjjrF5\n9NdeC3Xruo5MRFIpkdYKGuGH0Pffw8SJluhXr7b+Nrm58LOfuY5MRIJMI/yQ2LYNXn0Vxo6FWbMg\nEoE+faBzZ7UlFpHERvhK+AG2axfk51uSnzLF6vE9elh9vl4919GJSJAo4YfQtm120jUnx6ZUNmtm\nSb57dzj6aNfRiUhQKeGHxObNVq6ZNMnKNWedBVdeabcmTVxHJyJhoIQfULGYrf+amwvTp8OSJdCx\no7U4uOwyOPJI1xGKSNgo4QfI5s1Wj8/NtVuNGnDJJXDxxdCpExx2mOsIRSTMlPAdKiiA+fOt5fCc\nOfDuu3D++dbm4Fe/ghNPhKwg/vZFJJSU8FNoyxZ44w2IRi3Br1oFZ58NHTrYrV07+MlPXEcpIulK\nCd8nO3fCypXWiOzNN+3nxo1wzjk2P75DB2jTRr1rRCR1lPCToLDQTrCuXGm3t96CFSvghBMsqbdp\nAz//OZx2GlTXdcsi4kgiCT/V7ZEPAp4GFgCzgWYpPv5+FRbC8uUwfjxkZ8NVV1md/aijrNvkggWW\n5IcOtRWi3nkHRo+Gm2+GM87Yk+yj0ajL/4wqCXPsoPhdU/zBl+ox6ZVATeBcoA0w3HvMd7t3w+ef\nw4YN8Omn8NFH8MEHtn7r2rVWg2/e3JL8ySfbhU4PPmj3KzJyj0ajRCIRv/4zfBXm2EHxu6b4gy/V\nCf88INfbXgycXdk3KiqyUXlhIXz7LXz9tfWBL7l9/TV8+aUl9w0bYNMmm99+7LF2a9LETqr27GlJ\nvVEjOEjLwYhIGkt1wq8DfB93vwgr8xTHv+iXv7QRefxt505L7gUF9nPHDltwu1YtawX805/a7cgj\n7WeLFnDeedC4sSX4Y45RkzERyWypPmk7HFgEvOTd3wAcW+o16whQbV9EJCTWA81dBxGvG/BPb7st\n8KrDWERExEdZwEjgDe92kttwRERERETEF4Gdn19BbbD4w6YG8G9gLjZ7qovbcCqsGvAcMB+YB5zm\nNpxKOwo7rxXGb77LsL/92cAYx7FU1H1Y7lkC9HYcS0X1Zs/vfRGwDZscE2jdsH+wYEkzx2EslXUP\nsBL7wwmbPsCj3nY94GN3oVTKFcBob7sD4fz7qQFMAtYQvoR/CJbwwygCTPG2DwMecBdKlT0J3OQ6\niEQMB7rH3f/UVSBV0A07Q77QdSCVcBhQy9s+AjvbHzbVvJ+92TMxIEweBy7CRmphS/htsA+qGUC+\ndz8shgJ/wQYJrwOt3YZTaWcTourCKOBXcfc/JvVtH5KhKeFM+CVqY3/017oOpJKeB74Dfuk4jorq\nAwzxtmcDLdyFUik/A37jbZ+ITa0Oy7/fUdjFoNWxD9o1bsOptInYt9tQGA5cHXd/g6tAqqgp4U34\nx2I1zD6O46iqBsBHQJiaUc8Boliy34zVYhu4DKiCamJlnRKLgUaOYqmoYcCdcfffBsK25lxd4F3X\nQVREuszPb0o4E34DYDXQ0XUgldQLO/EGdsLqQ+Bgd+FUSRhLOv2Af3jbx2B/S2EZ4XcG8rztY4C1\nBLOLcHkuB55I5IVBaeg7Cfsa/oZ3v6/DWKoqOL2dEzcYOBy437sBXAJsdxZRxbyMlXPmYCc/7wB2\nuAwow4zBBmxzvft9KdUuJcBeBdoDb2IfUrcSvn/DJxHO824iIiIiIiIiIiIiIiIiIiIiIiIiIiKS\nehHgS+xipSh2HUfJFdtnAH8sZ9/zgdN9jK20E7G+LYnoB1zgYywiIqHTARgbd/8wYCmW7A/keeBi\nH2LanxygfoKvrYY1IQvL1aoSEkG50lakMkpfAr8VeAb4NdZfpD/QA7sKtBnWX+cJYBWW7M/0tq8A\numIfGF9729cBl3r7NAMeAl7AOkE+hiXjjd7rTvTeNwv4BrgR+D4urhbe67/F2m+MBz7xtsdhzcda\nYVd9DgGKgOXYZf9TK/vLERFJJxH2HuGD9RUZyZ7Rfy2se+MR3q2H97p/Yu2Is7B2EiUfHrnAuVib\n5VzvseZYfxiw5lol3Sz7Yol6IXCy99hvgAdLxfTbuMeaYmWo2lgPox+wD6eDgS/i9rkBayookjQa\n4Uu6acre3VYLgQFYG9w6wIulXh8DdmEfDoVAY6wfD1hyB1ufoaQbZAPgfW+7pOHfKdiHDN6+H5Q6\nxhHAprjCPJMcAAAA80lEQVT7HwIF3nE3AVviYinxOarjS5Ip4Us6qYOt+HMVe9rzNsQWteiGJe1P\nsOUci7FaeUuspNMWOBQ7B1Ay2i+ridZn2Ih/HTAI6664BuvY+SnWiOuIUvt8iXViLJFIc6763n4i\nSaOEL2EWw0bBs7G6d3WsPLMWS7AxrEzSEJvBUwQ84v1cjPVC74nV/udi9ftl7EnO8Ym5ZLsfthxn\nMZb8H2fPh0h173U3loozyt7ta8t639LbbdhTUhIRkRCZgi1Snojq2MpjYevLLiIi2Endvyb42luA\nTj7GIiIiIiIiIiIiIiIiIiIiIiIiIiIiIpX3/0GMuT6tJ4ztAAAAAElFTkSuQmCC\n",
       "text": [
        "<matplotlib.figure.Figure at 0x7fe05ac8b890>"
       ]
      }
     ],
     "prompt_number": 76
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "### Exercise\n",
      "\n",
      "Dress up your graph of trigonometric functions. Including the function being plotted in the legend."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "## Multiple Plots in the Same Figure\n",
      "\n",
      "So far, we have only needed the simplest utilities that `matplotlib` provides. A single _figure_ on a single _axis_. To get more than one plot we have to use the `subplot()` method.\n",
      "\n",
      "You can divide a figure into many subplots. For instance, we could divid a figure vertically into two subplots which would looke like\n",
      "            \n",
      "<img src='subplot-vertical.png'>"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "The nomenclature here is `subplot(<row number>, <column number>, <axes number>)`, where the axes number is counted _clockwise_ from the top left.\n",
      "\n",
      "We can start to populate the subplots, like with"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "ax1 = pyplot.subplot(1, 2, 1) # tell matplotlib which subplot we want to work on\n",
      "pyplot.plot(x, cosy)\n",
      "pyplot.show()"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "metadata": {},
       "output_type": "display_data",
       "png": "iVBORw0KGgoAAAANSUhEUgAAAL8AAAD/CAYAAACgsgdqAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz\nAAALEgAACxIB0t1+/AAAFspJREFUeJzt3XtwVPXdx/F3ICQIEkSsgoikoIAIBOSSyM1FCygUq1ha\ntfVarfXp6FPtjO3jo490enVGmdZ5ntaKaEurqK1SabmEcgl3QkgwgIgYKeCFKiOBgBBI4Dx/fHdl\nSTbZ3bPn7O9cvq+ZHTds8jtfdz979vs75+w5oJRSSimllFJKKaWUUqFXDKxI8O9TgY3AOuCerFak\nVBY8AmxBAh6vHfAe0Dl6fyNwfnZLUyq5Nhn8bQ0wDchp8u+XRR87BDQAa4BxGSxHKVdkEv43gMYE\n/16ABD/mMPIpoJSnZBL+lhwCOsX93AmodWE5SmXEjfDvAC4FugB5SMuzvukv9ezZxwL0pje3bjUk\n4UT4reh/bwHuRfr8h4FSZDI8G9jX9I8++OB9LMviyBGL6dMtxo+3OHbMwrIyuz3xxBMZj5Gtcf1U\nq5vjPvDAE/TsafHkkxanTjkzJtDH7fDvBkZF788FZkXv/wMYCQwHftfaAB07wty50K0bfPvbYFmt\n/bYKmgMH4KWX4NFH4ZFHIKfp5hMXudH2pK1tW3jxRdi9G555xnQ1KlssC+6+Gy69FL73vewvPzf7\ni0wsPx/+8hcYMQImT5YnxI5IJOJoXW6O66da3Rj3T3+CXbvgqaecHTdVWfyQacayEvQ4M2fCokWw\nZEl2PwJVdh04AP37Q2kpDB3q/Pg5Ep5WE+SJtifegw/CJ5/AvHmmK1Fu+tnPYNo0d4KfKs+t+QEW\nLIAf/Qiqq2U+oILl/fehuBjefhsuuMCdZfhyzQ/S8xcUwKuvmq5EueFXv4Lvf9+94KfKk2t+gKVL\n4YEHZO3QxpNvUWXHRx/BoEGwcyecd557y/Htmh/gmmtkC1BpqelKlJN+/Wu4/XZ3g58qz675AebM\ngT//Wbb8KP87fBh69ZK5XM+e7i7L12t+gJtvhm3b5Kb87+WXIRJxP/ip8nT48/JkD+Dzz5uuRGXK\nsuD3v4f77jNdyWmebntA9gAWF8OHH8ocQPlTRQV885tQU5OdDRi+b3sAeveWrQNvvmm6EpWJ556D\ne+/11pY7z6/5QY76mzNHt/z4VX09XHghbN0KPXpkZ5mBWPOD7AYvL5fDHpT/LFoERUXZC36qfBH+\ns86Cr34V/vpX05UoO15+GW691XQVzfki/CCbPV95xXQVKl11dbKf5qabTFfSnG/CP3EibN8uW32U\nf/ztb3DVVXDuuaYrac434c/LgxtukC+8KP947TX51PYi34QfYPp07fv95MgRWLUKpkwxXUlivgr/\n+PFylOf+/aYrUakoLYWSEujs0VOW+Sr8+fkwYYJ82UV535tvwte+ZrqKlvkq/ADXXw/z55uuQiXT\n2AgLF8rr5VW+C//kybBsmew1VN61Zo0cvuyVIzgT8V34u3aFIUNg+XLTlajWeL3lAR+GH+SjVA90\n87aFC2WvvJf5MvyTJ8PixXpqQ6/avRtqa+UT2st8Gf7+/eHUKfkStPKeJUtkq5yXDl9OxOPlJZaT\nI4c76Hd7vam0FCZNMl1Fcr4MP2j4vaqxUTZGTJxoupLkfBv+a66BlSvhxAnTlah4GzfKJs5u3UxX\nkpxvw3/eedCvH6xvds0XZVJpqT/W+uDj8IO2Pl7kl34ffB7+SZP0e71ecvCgHHg4ZozpSlLj6/CX\nlMC778qTrsxbvVpOM+OXU8z4Ovx5efJkr15tuhIFUFYmZ2TzC1+HH+TJXrnSdBUK5HXQ8GdRJCJr\nHGXWwYPSgo4YYbqS1Pk+/CNGyJN+6JDpSsJtzRp/9fsQgPDn58PIkfLkK3P81u9DAMIP2vp4gYbf\nEA2/WX7s9yEg4R85EnbskLODqezzY78PAQl/fr6sdbTvN6OsTM7K5jeBCD/A2LEaflPWroVx40xX\nkb7AhH/UKHkRVHYdOwZbtviv34cAhb+kBCoroaHBdCXhUlkJAwZAhw6mK0lfYMLfuTP06QObN5uu\nJFzWrYPRo01XYY/d8LcBngXWASuAPk0efwjYFn1sBdDXboHpGDVKXgyVPWvXyvPuR3bDfwOQB4wC\nfgw83eTxK4DbgPHRW1bOszB6tPb92WRZsrIJW/hHA4uj98uB4U0eHwY8CqxG3hxZEQu/ns8nO2pq\npNe/6CLTldhjN/wFQPwupZNNxpoL3AdcDYwBsnKG9sJCCf6ePdlYmvJzywOQa/Pv6oBOcT+3AU7F\n/fwbTr85FgBDo/89w4wZM764H4lEiGR4cEhOzum1f2FhRkOpFHip5SkrK6MsS8e4TANejN4v4cxg\ndwb2AB2R66D+Bbg2wRiWG55+2rLuv9+VoVUTAwZYVmWl6SoSA5I2v3bX/POACUBsenkXcAtwNjAL\n6fNXAMeBpZyeH7hu9Gi5YLVyV20t7N0LgwebrsQ+X1yBPR0nTsiV/z7+GAoKHB9eRS1aBE89JddK\n8KLAXIE9HXl5cnbgTZtMVxJsXur37Qpc+EEOcS4vN11FsG3YIIeU+Fkgw19cLOeMVO44dQoqKmQl\n42eBDH9sza87u9yxc6fMq770JdOVZCaQ4S8slFNlf/SR6UqCaeNG/6/1IaDhz8nR1sdNGn6P00mv\nezT8HqdrfnccPw7btsHQoaYryVxgwz9ihHzL6ORJ05UES3U19O0LHTuariRzgQ1/ly7QvTu8847p\nSoIlKC0PBDj8oH2/GzZulJYyCAIdfu37nVdermt+X9A1v7Nqa+WAwQEDTFfijECHv6gI3nsPPv/c\ndCXBsGkTXHEFtG1ruhJnBDr8+fkwcCBUVZmuJBiCNNmFgIcf5MXSvt8ZGn6fGT5ctverzFhWsCa7\nEILwDxum4XfChx/KG+Dii01X4pzAh79/fzm6U8/dn5lNm+RTNMfkF18dFvjw5+bKVh+d9GamslI+\nRYMk8OEHbX2cUFUlmzmDJBThHz5cv9CeCcvSNb9v6Zo/Mx9/LG8Av56TsyWhCH///rBvn16o2q7K\nSml5gjTZhZCEv21bnfRmIogtD4Qk/CAvnvb99gRxsgshCr/u6bVP1/w+p2t+e/btk+/t9upluhLn\nhSb8/frBJ5/IMekqdbGWJ2iTXQhR+Nu2lRPY6qQ3PUFteSBE4Qft++0I6mQXQhZ+7fvTp2v+gNA9\nven59FM4fBh69zZdiTtCFf6+fWH/fp30pirIk10IWfhjk15d+6cmyC0PhCz8oJPedFRVafgDRSe9\nqYsd0BZUoQy/rvmT++wzOHAALrnEdCXuCV34ddKbmqoqOQ15mwAnJMD/a4m1aaN7elMR9MkuhDD8\noK1PKoI+2QUNv2pB0Ce7oOFXCdTWyt7dvn1NV+KuUIY/dnjzwYOmK/GmzZvla59BORtzS0IZ/th3\nejdvNl2JN4VhsgshDT9IP6utT2JhmOxCiMOvfX/LwjDZBQ2/aqKuTk5S1b+/6UrcZzf8bYBngXXA\nCqBPk8enAhujj99juzoX9e8vL7KeyOpMmzfD4MFygt+gsxv+G4A8YBTwY+DpuMfaATOBCcBVwHeB\n8zOo0RW5ufIiv/WW6Uq8JSwtD9gP/2hgcfR+OTA87rHLgBrgENAArAHG2S3QTTrpbS4sk12wH/4C\nIP5yDyfjxipAgh9zGOhsczmu0r6/uTCt+e12dnVAp7if2wCnovcPNXmsE5DwGMoZM2Z8cT8SiRCJ\nRGyWY8+wYfDkk1ldpKcdOQJ79/rzOrtlZWWUlZVlZVnTgBej90uABXGPtQN2Al2QecEmoHuCMSzT\nGhosq0MHy6qrM12JN6xebVkjR5quwhmAlSzEdtueeUA9sBaZ7D4E3ALci/T5DwOlyNae2cA+m8tx\nVW4uDBqke3pjwtTygP22xwLub/JvO+Pu/yN687xhw2SSN86TU/LsqqqCMWNMV5E9od3JFaNbfE4L\nyzE9MaEPv27xEUePwq5dMHCg6UqyJ/Thv/xy2LNHtnSEWXW1bOXJyzNdSfaEPvzt2skbIOx7eoN8\nQtqWhD78oK0PhK/fBw0/IGu8sJ/NQdf8IRX2NX99PezcKfs8wkTDj2zh2LULPv/cdCVmbNkiX1Zv\n3950Jdml4Ue2cAwYIFs8wihMR3LG0/BHhbn1CdthDTEa/qjYYQ5hpGv+kAvrYQ7Hj8M778i32sJG\nwx81aBDU1MCxY6Yrya6334Y+faBDB9OVZJ+GPyo/X77UHrZJb1j7fdDwnyGMk96w9vug4T9DGMMf\nxsMaYjT8ccJ2mENDg/T8RUWmKzFDwx9n8GDZzV9fb7qS7Ni+HXr1grPPNl2JGRr+OO3by27+LVtM\nV5IdYZ7sgoa/mTD1/WGe7IKGv5kwhV/X/OoMYQl/Y6O0d0OHmq7EHA1/E4MHw7vvym7/INuxA3r0\ngIIC05WYo+Fv4qyz5KrjW7earsRdYe/3QcOfUBhan7D3+6DhTygs4dc1v2om6OFvbJRTtWj4VTNF\nRXKM+4kTpitxx/btcNFF0NmTV03IHg1/Ah06QO/esG2b6UrcUVEBI0aYrsI8DX8Lgtz6aPiFhr8F\nGv7g0/C3IKjhr6+X+cyQIaYrMU/D34KiIjnWPWiT3rfegn79ZGde2Gn4W3D22VBYKFtGgkRbntM0\n/K0IYutTUQEjR5quwhs0/K0Iavh1zS80/K0IWvjr6uCDD+RiHErD36ohQ2RHV0OD6UqcUVkpE/lc\nu9fgDBgNfys6dZIveAdlT6+2PGfS8CdRUgLl5aarcIaG/0wa/iSKi2HDBtNVOEPDfyYNfxIlJcEI\n/6efwsGD8i01JTT8SVx+OXz0EdTWmq4kMxUVMHw4tNFX/Av6VCSRmyuh2bjRdCWZ2bBBPsXUaRr+\nFASh9Vm/Hq680nQV3qLhT0Fxsb+3+Jw8KW2PrvnPpOFPQSz8lmW6Enu2b4du3aBrV9OVeIuGPwXd\nu8sOr/feM12JPevX61o/ETvhPwt4HVgFLADOS/A7vwE2ASuA5YDvzwvm551d2u8nZif89wPVwDhg\nDvBYgt+5ApgIjAeuBursFugVft7ZpeFPzE74RwOLo/cXA19JMOalwCxgDXCX7eo8xK9bfA4ckP0U\neiRnc8mO7/sO8IMm//YJp9fkh4GmZ3/pADwDzIyOvwJpgXx99suhQ+XkrkeP+uuyneXlckiDHsnZ\nXLKnZHb0Fu91oFP0fifgYJPHjyLhj13cZzlQRILwz5gx44v7kUiESCSSQslmtG8PAwfKCV7HjDFd\nTerC0vKUlZVRVlaW1t/k2FjOw0jofwLcDIwFvh/3+GXAXKTvbwuUAfcA7zQZx7J8tu3wBz+ACy+E\nRx4xXUnqJkyABx+EqVNNV5JdOTk5kCTfdnr+3wGXA6uRUP8k+u8PAVORkM8B1iMtzx9oHnxfGjMG\n1qwxXUXqTp6UwzJ0M2didtb8TvHdmn/fPml99u/3xwFi27bBjTf6d/9EJtxa84dW9+7QpYuc9MkP\nVq/21/wk2zT8aRo7VkLlB6tWwbhxpqvwLg1/mvzS91uWvEnHjjVdiXdp+NM0Zow/1vy7d8uEt08f\n05V4l4Y/TX37wrFjsHev6UpaF2t5ckxu0vA4DX+acnL80fpoy5Ocht8Gv4RfJ7ut0/Db4PUtPv/+\nt5ytYeBA05V4m4bfhiFDZEJ54IDpShJbswZGj/bHjjiT9OmxoV07GDVKJpVepC1PajT8Nl19NSxf\nbrqKxFau1MluKjT8Nl1zDSxbZrqK5vbvl5ZMT0uYnIbfpqFD4eOPZXLpJStWyFpfv7ySnIbfprZt\n4aqrJGxesnSpfCqp5DT8GfBi379smYY/VRr+DHit7//Xv+DIEd2+nyoNfwYGDIDPP5fQeUFsra/H\n86RGw5+BnBxpfbzS92vLkx4Nf4a+8hX45z9NVyHH7y9fruFPh4Y/Q9deC0uWyLHzJlVXy/lECwvN\n1uEnGv4M9egBF11k/jyeCxfClClma/AbDb8DJk+W8Jm0cKHUoVKn4XfAddfBokXmlv/ZZ7Bli+x0\nU6nT8Dvgyith1y5zhzosWQKRiJxSUaVOw++Adu1kq8/ixcl/1w3a8tij4XfI5MmwYEH2l3vypLzp\nrrsu+8v2Ow2/Q6ZMke399fXJf9dJGzbI9bZ69crucoNAw++Q88+HoqLs7/B64w2YNi27ywwKDb+D\nbroJXn89e8uzLFneTTdlb5lBouF30I03wt//Dg0N2VleVZVMtgcNys7ygkbD76CePeGSSyDNC4TY\nFlvr61Gc9mj4HZat1kdbnsxp+B329a/LJNTt1qe6Go4fh+HD3V1OkGn4Hda7t5zM1u3DHebMgdtu\n05YnExp+F9xxh4TTLY2N8PLLEn5ln4bfBdOny1kU3Dqd4ZIl8OUvyyeMsk/D74JzzpEvubz6qjvj\n//GPcPvt7owdJno1RpcsWgSPPw4VFc725Z9+Cv36wfvvw7nnOjdu0OjVGA2aOBFqa53/htfzz8vm\nTQ1+5nTN76KZM6GyEl56yZnxGhul158/X06XqFqma37D7r5bjrV36ksu8+fDxRdr8J2i4XfROefA\nrbfCM89kPpZlwZNPwkMPZT6WEtr2uGzPHrjiCti5E7p2tT9OaSk8/DBs3apXXEmFtj0e0KuXHPIw\nc6b9MSwLfvpTeOwxDb6TdM2fBXv2wLBhcjxOjx7p//28ebLZtLpaTo2ukktlza/hz5LHH4eaGpg7\nN72/q6+XE+LOmqWnIkyHht9Djh6Fyy6D2bPlTA+peuwx2L5djhRVqXO7578RaGkL9r1ABbAe0JPo\nAR06yNr7zjvlulmpWLtW3iy//a2rpYWW3fD/BvgFid9Z3YAHgFHAJOCXQJ7N5aStzKWvUTkx7sSJ\n8K1vye3EidbH3LsXvvENeO45OTtDOrz8HGRz3GTshn8tcD+Jwz8y+ngDUAfUAINtLidtXn+Bfv5z\n6NgRbrkFli5NPObevTBpEvzwhzB1avrL8PpzkK1xk0kW/u8AW5vchgGvtfI3nYBDcT8fBjpnUGOg\n5ObCK6/IVpsXXpAD32JOnYLXXoOSEvjud2W7vnJPsgtWzo7e0lGHvAFiOgG1aY4RaPn5crjz9dfL\nGR8KCqB7d9ixQ/776qt6EWmviwCJNtxdAGwB8pE1/jsk7vlrAEtvenPpVkMSmVyqOLaQmIeiC/w7\n8AywGmmrHgVOJPj7SzJYtlJKKaWUD7QBngXWASuAPg6OXRwd0yntgD8Bq4BywMaGx4TaAi8Aa5D2\n8HKHxo05H/gAcOor7lXI87qC9DeAtOa/kBxUAHc4NOYdnK51A3AMKHBo7IxNQ154kLD+zaFxH0Em\n2uscGg/gTiB2PGYXYI9D434NeD56/yqcew5A3rDzgB04E/72SPidFgHmR+93BH7iwjL+F7jHhXFt\nexr4RtzPHzo07jRkEr3eofFAXpSzo/e7Au87OHbs+Mw7gBcdHPfXwERkzedE+IuRN1IpsCz6sxN+\nAfwceeMvR/YfOWk4znYBjpgFXBv38x6c+15BIc6GP6YT8gLd7PC4f0B2CE5waLw7gf+O3l8B9HNg\nzIHIzk6AS5Etek68XrOAxcgWx77IG8xJbyCfqp7yNDA97ucPHBy7EOfD3xPpSe90eNyYC4DdwFkO\njLUSKEOCX4v0vBdkOGYe0vrElAM2vpXQzC+B+H3YbwHnOTAuwDnANofGctQ0Tn/MlwBOXsmqEGfD\nfwGyk268g2MC3IZM9kAmY7uQnYJOcqrtuQ/4v+j9C5Hnw4k1/xRgSdy47+HcIfbXIwdftiqTnVx2\nzUM+5tdGf77L4fGt5L+SskeRvdT/E70BXAdkeuWtvyItz0pkgvqfwPEMx3TLbGRltSr6813AKQfG\nXQCMAzYib6b/wLnXri/Ozs+UUkoppZRSSimllFJKKaWU8ob/BzJioTkt4PfkAAAAAElFTkSuQmCC\n",
       "text": [
        "<matplotlib.figure.Figure at 0x7fe05ad47550>"
       ]
      }
     ],
     "prompt_number": 77
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "## Exercise\n",
      "\n",
      "Construct a two-axes plot with `cosy` on the left and `siny` on the right. Hint: wait to say `pyplot.show()` until you're ready for the graph to appear."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [],
     "language": "python",
     "metadata": {},
     "outputs": []
    }
   ],
   "metadata": {}
  }
 ]
}