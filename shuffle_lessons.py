#!/usr/bin/env python

import subprocess
import argparse as ap

def main():
    description = "Shuffle two lesson numbers"

    parser = ap.ArgumentParser(description=description)
    
    current = "the current lesson number"
    parser.add_argument('current', type=int, help=current)
    new = "the new lesson number"
    parser.add_argument('new', type=int, help=new)
    
    args = parser.parse_args()
    if args.current >= args.new:
        raise ValueError("Current value must be lower than new value")
    
    old = range(args.current, args.new + 1)
    # move last of old to first of new and decrement each other 
    new = [old[-1]] + [x - 1 for x in old[1:]]
    fmt = '{:0>2d}'
    cmd_t = """for f in `ls {}*`; do git mv $f "{}-${{f#*-}}"; done"""
    cmd = cmd_t.format(fmt.format(old[0]), fmt.format(99))
    print("Executing: {}".format(cmd))
    subprocess.check_call(cmd, shell=True)
    for a, b in zip(old[1:], new[1:]):
        a = fmt.format(a)
        b = fmt.format(b)
        cmd = cmd_t.format(a, b)
        print("Executing: {}".format(cmd))
        subprocess.check_call(cmd, shell=True)
    cmd = cmd_t.format(fmt.format(99), fmt.format(new[0]))
    print("Executing: {}".format(cmd))
    subprocess.check_call(cmd, shell=True)
    
if __name__ == '__main__':
    main()
