#!/bin/bash

#
# Prepare a local repository for a fetch/merge from a remote
#
# Detect all files that will be updated between the local HEAD of a branch and
# the remote's HEAD. Run `git checkout -- <file>`, erasing local
# modifications. Then merge remote/branch.
#
# Author: Matthew Gidden
#

set -x
set -e

remote="origin"
branch="master"

git fetch $remote
# remote modified files (that is, any files updated between local HEAD and
# remote/branch HEAD)
git diff --diff-filter=M --name-only HEAD $remote/$branch | xargs git checkout --
git merge $remote/$branch
