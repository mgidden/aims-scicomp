AIMS Scientific Computing
=========================

Welcome to the AIMS course on Scientific Computing.  This course seeks
to teach you the basics of professional software development in the context
of science.  The subjects we will cover include,

* Programming and Computer Science Basics - Python
* The Shell - bash
* Version Control - git
* Best Practices - testing

Course Provenance
=================
Many of the materials presented here come may have come originally from 
other sources.  Files or directories for such materials are denoted by 
the cooresponding prefix. 

* **Software Carpentry (swc-):** [CC BY 3.0](http://creativecommons.org/licenses/by/3.0/)
* **The Hacker Within (thw-):** [CC BY 3.0](http://creativecommons.org/licenses/by/3.0/)

The original course was developed by Anthony Scopatz. A number of revisions and
additions have been incorporated by Matthew Gidden.
